variable "cidr_vpc" {
  default = "192.168.0.0/16"
}

variable "vpc_name" {
  default = "Test-VPC"
}

variable "cidr_subnet" {
  default = "192.168.10.0/24"
}

variable "availability_zone_name" {
    default = "ap-south-1a"  
}

variable "subnet_name" {
  default = "PUB-SUBNET"
}

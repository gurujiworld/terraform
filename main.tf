# Create VPC
resource "aws_vpc" "demo-vpc" {
  cidr_block       = var.cidr_vpc
  instance_tenancy = "default"

  tags = {
      Name = "${var.vpc_name}"
 }
    
}

# Create Subnet in the VPC

resource "aws_subnet" "demo-pub" {
  vpc_id     = aws_vpc.demo-vpc.id
  cidr_block = var.cidr_subnet
  availability_zone = var.availability_zone_name
  
  tags = {
    Name = "${var.subnet_name}"
  }

}

# Create Internet Gateway For Demo-VPc 

resource "aws_internet_gateway" "demo-gw" {
  vpc_id = aws_vpc.demo-vpc.id

  tags = {
    Name = "Demo-gw"
  }
}

# Create Route Table 
resource "aws_route_table" "demo-rwt" {
  vpc_id = aws_vpc.demo-vpc.id
  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = aws_internet_gateway.demo-gw.id
  }

  tags = {
    Name = "Demo-RWT"
  }
}

# Route Table Association 

resource "aws_route_table_association" "a" {
  subnet_id      = aws_subnet.demo-pub.id
  #subnet_id      = aws_subnet.demo-pub11.id
  route_table_id = aws_route_table.demo-rwt.id
}

#resource "aws_route_table_association" "b" {
  #subnet_id      = aws_subnet.demo-pub10.id
#  subnet_id      = aws_subnet.demo-pub11.id
#  route_table_id = aws_route_table.demo-rwt.id
#}

# Create Sequrity Group to Allow 22 80 8080 443

resource "aws_security_group" "allow_Web-SSH" {
  name        = "allow_tls"
  description = "Allow TLS inbound traffic"
  vpc_id      = aws_vpc.demo-vpc.id

  ingress {
    description      = "HTTPS from VPC"
    from_port        = 443
    to_port          = 443
    protocol         = "tcp"
    cidr_blocks      = ["0.0.0.0/0"]
    
  }

  ingress {
    description      = "HTTP from VPC"
    from_port        = 80
    to_port          = 80
    protocol         = "tcp"
    cidr_blocks      = ["0.0.0.0/0"]
    
  }

  ingress {
    description      = "SSH from VPC"
    from_port        = 22
    to_port          = 22
    protocol         = "tcp"
    cidr_blocks      = ["0.0.0.0/0"]
    
  }

  egress {
    from_port        = 0
    to_port          = 0
    protocol         = "-1"
    cidr_blocks      = ["0.0.0.0/0"]
    ipv6_cidr_blocks = ["::/0"]
  }

  tags = {
    Name = "Terraform_SG"
  }
}

resource "aws_security_group" "worker_group_mgmt_one" {
  name_prefix = "worker_group_mgmt_one"
  vpc_id      = aws_vpc.demo-vpc.id

  ingress {
    from_port = 22
    to_port   = 22
    protocol  = "tcp"

    cidr_blocks = [
      "10.0.0.0/8",
    ]
  }
}

resource "aws_security_group" "worker_group_mgmt_two" {
  name_prefix = "worker_group_mgmt_two"
  vpc_id      = aws_vpc.demo-vpc.id

  ingress {
    from_port = 22
    to_port   = 22
    protocol  = "tcp"

    cidr_blocks = [
      "192.168.0.0/16",
    ]
  }
}

resource "aws_security_group" "all_worker_mgmt" {
  name_prefix = "all_worker_management"
  vpc_id      = aws_vpc.demo-vpc.id

  ingress {
    from_port = 22
    to_port   = 22
    protocol  = "tcp"

    cidr_blocks = [
      "10.0.0.0/8",
      "172.16.0.0/12",
      "192.168.0.0/16",
    ]
  }
}



